//
//  Object.swift
//  SyncThreads
//
//  Created by Nazar Starantsov on 04.11.2019.
//  Copyright © 2019 Nazar Starantsov. All rights reserved.
//

import Foundation


struct SafeArray<T> {
    private var array = [T]()
    
    let queue = DispatchQueue(label: "com.asad32rfds")
    
    mutating func append(_ value: T) {
        queue.sync {
            self.array.append(value)
        }
    }
    
    mutating func remove(at index: Int) {
        queue.sync {
            self.array.remove(at: index)
        }
    }
    
    func get(itemAt index: Int) -> T {
        queue.sync {
            return self.array[index]
        }
    }
}
